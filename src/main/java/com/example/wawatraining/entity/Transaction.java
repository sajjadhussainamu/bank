package com.example.wawatraining.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
public class Transaction implements Serializable {

	private static final long serialVersionUID = 198798797979L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trans_Id")
	private long id;

	@Column(name = "from_account")
	private long fromAccount;

	@Column(name = "to_account")
	private long toAccount;

	@Column(name = "amount")
	private float amount;

	@CreationTimestamp
	@Column(name = "create_date")
	private LocalDateTime createDate;

	@UpdateTimestamp
	@Column(name = "modify_date")
	private LocalDateTime modifyDate;

}
