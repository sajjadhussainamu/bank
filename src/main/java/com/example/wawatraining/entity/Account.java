package com.example.wawatraining.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
public class Account implements Serializable {

	private static final long serialVersionUID = 1986668797979L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "account_number")
	private long accountNumber;

	@Column(name = "opening_balance")
	private float openingBalance;

	@Column(name = "status")
	private int status;

	@OneToOne
	@JoinColumn(name = "Cust_id")
	private Customer customer;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modify_date")
	private Date modifyDate;

	@Override
	public String toString() {
		return "Account [id=" + id + ", accountNumber=" + accountNumber + ", openingBalance=" + openingBalance
				+ ", status=" + status + ", customer=" + customer + ", createDate=" + createDate + ", modifyDate="
				+ modifyDate + "]";
	}

}
