package com.example.wawatraining.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawatraining.dto.CustomerRequestDto;
import com.example.wawatraining.dto.CustomerResponseDto;
import com.example.wawatraining.entity.Customer;
import com.example.wawatraining.service.RegistraionService;

@RestController
@RequestMapping("/registraions")
public class RegistraionController {

	@Autowired
	RegistraionService registraionService;

	@PostMapping("/process")
	public ResponseEntity<CustomerResponseDto> registraionsProcess(
			@Valid @RequestBody CustomerRequestDto customerRequestDto) {

		CustomerResponseDto customerCheck = registraionService.getCustomerByUsername(customerRequestDto.getUsername());

		if (customerCheck != null)
			return new ResponseEntity<>(customerCheck, HttpStatus.BAD_REQUEST);

		Optional<Customer> customerCheckByno = registraionService
				.getCustomerByPhoneNumber(customerRequestDto.getPhoneNumber());
		if (customerCheckByno.isPresent())
			return new ResponseEntity<>(customerCheck, HttpStatus.BAD_REQUEST);

		CustomerResponseDto customerResponseDto = registraionService.createCustomer(customerRequestDto);

		return new ResponseEntity<>(customerResponseDto, HttpStatus.CREATED);
	}

	@GetMapping("/getByPhoneNumber")
	public ResponseEntity<Integer> getByPhoneNumber(@RequestParam long phoneNumber) {
		Optional<Customer> customerCheck = registraionService.getCustomerByPhoneNumber(phoneNumber);

		if (customerCheck.isPresent() && customerCheck.get().getAccount().getAccountNumber() > 1)
			return new ResponseEntity<>(HttpStatus.OK);

		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping("/checkUser")
	public Boolean getCustomerByUsername(@RequestParam("username") String username) {

		CustomerResponseDto customerCheck = registraionService.getCustomerByUsername(username);
		if (customerCheck == null)
			return false;
		return true;
	}

}
