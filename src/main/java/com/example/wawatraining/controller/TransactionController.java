package com.example.wawatraining.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawatraining.dto.TransactionsResponseDto;
import com.example.wawatraining.entity.Account;
import com.example.wawatraining.entity.Customer;
import com.example.wawatraining.service.AccountService;
import com.example.wawatraining.service.RegistraionService;
import com.example.wawatraining.service.TransactionService;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

	@Autowired
	TransactionService transactionService;

	@Autowired
	AccountService accountService;

	@Autowired
	RegistraionService registraionService;

	@PostMapping("/process")
	public TransactionsResponseDto transactionsProcess(@RequestParam long formAccount, @RequestParam long toAccount,
			@RequestParam float amount) {
		TransactionsResponseDto transactionResponseEntityDto = new TransactionsResponseDto();
		Optional<Account> fromAccountDetail = accountService.getAccountByAccountNumber(formAccount);

		if (!fromAccountDetail.isPresent()) {
			transactionResponseEntityDto.setStatusCode(400);
			transactionResponseEntityDto.setStatusMessage("Account Not in list Please check your account number");
			return transactionResponseEntityDto;
		}

		if (fromAccountDetail.get().getOpeningBalance() < amount) {
			transactionResponseEntityDto.setStatusCode(400);
			transactionResponseEntityDto.setStatusMessage("Insufficient Balance");
			return transactionResponseEntityDto;
		}
		Optional<Account> toAccountDetail = accountService.getAccountByAccountNumber(toAccount);
		transactionResponseEntityDto = transactionService.transactionsProcess(fromAccountDetail.get(),
				toAccountDetail.get(), amount);
		transactionResponseEntityDto.setStatusCode(200);
		transactionResponseEntityDto.setStatusMessage("Transaction successfully done");
		return transactionResponseEntityDto;
	}

	@PostMapping("/byMobileNumber")
	public TransactionsResponseDto byMobileNumber(@RequestParam long fromNumber, @RequestParam long toNumber,
			@RequestParam float amount) {
		
		Optional<Customer> fromCustomerDetail = registraionService.getCustomerByPhoneNumber(fromNumber);
		Optional<Customer> toCustomerDetail   = registraionService.getCustomerByPhoneNumber(toNumber);

		/*TransactionsResponseDto transactionResponseEntityDto = new TransactionsResponseDto();
		Optional<Account> fromAccountDetail = accountService
				.getAccountByAccountNumber(fromCustomerDetail.get().getAccount().getAccountNumber());
		if (fromAccountDetail.get().getOpeningBalance() < amount) {
			transactionResponseEntityDto.setStatusCode(400);
			transactionResponseEntityDto.setStatusMessage("Insufficient Balance");
			return transactionResponseEntityDto;
		}

		Optional<Account> toAccountDetail = accountService
				.getAccountByAccountNumber(toCustomerDetail.get().getAccount().getAccountNumber());
		transactionResponseEntityDto = transactionService.transactionsProcess(fromAccountDetail.get(),
				toAccountDetail.get(), amount);
		transactionResponseEntityDto.setStatusCode(200);
		transactionResponseEntityDto.setStatusMessage("Transaction successfully done");
		 return transactionResponseEntityDto;*/

		return this.transactionsProcess(fromCustomerDetail.get().getAccount().getAccountNumber(),
				toCustomerDetail.get().getAccount().getAccountNumber(), amount);

	}

}
