package com.example.wawatraining.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawatraining.entity.Customer;
import com.example.wawatraining.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	AccountService accountService;

	@PostMapping("/addAccount")
	public ResponseEntity<Customer> addingProcess(@RequestBody Customer customer) {
		accountService.createAccount(customer);
		return new ResponseEntity<>(customer, HttpStatus.CREATED);
	}

}
