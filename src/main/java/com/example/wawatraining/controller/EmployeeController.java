package com.example.wawatraining.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawatraining.dto.EmployeeRequestDto;
import com.example.wawatraining.dto.EmployeeResponseDto;
import com.example.wawatraining.dto.EmployeesResponseDto;
import com.example.wawatraining.entity.Employee;
import com.example.wawatraining.service.EmployeeService;

@RestController
@RequestMapping("/employees")
@Validated
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@PostMapping("")
	public ResponseEntity<String> saveEmployee(@Valid @RequestBody EmployeeRequestDto employeeDto) {
		if (employeeDto.getFirstName() == null)
			return new ResponseEntity<String>("Fisrt name not be Null ", HttpStatus.BAD_REQUEST);
		employeeService.saveEmployee(employeeDto);

		return new ResponseEntity<String>("Success", HttpStatus.CREATED);
	}

	@GetMapping("")
	// public ResponseEntity<List<EmployeeResponseDto>> getEmployees() {
	public EmployeesResponseDto getEmployees() {
		// List<EmployeeResponseDto> employeeResponseDto =
		// employeeService.getEmployees();
		EmployeesResponseDto employeesResponseDto = employeeService.getEmployees();

		// return new ResponseEntity<>(employeeResponseDto, HttpStatus.OK);
		employeesResponseDto.setStatusCode(200);
		employeesResponseDto.setStatusMessage("success");
		return employeesResponseDto;
	}

	@GetMapping("/{employeeId}")
	public EmployeeResponseDto getEmployeeById(@PathVariable long employeeId) {
		EmployeeResponseDto clientResponseDto = employeeService.getEmployeeById(employeeId);
		// clientResponseDto.setEmployeeId(employeeId);
		return clientResponseDto;
	}

	@PutMapping("/{employeeId}")
	public void updateEmployee(@PathVariable long employeeId, @RequestBody @Valid EmployeeRequestDto employeeDto) {
		// Employee emp = employeeService.getEmployeeById(employeeId);
		Employee emp = new Employee();

		emp.setFirstName(employeeDto.getFirstName());
		emp.setLastName(employeeDto.getLastName());
		emp.setAge(employeeDto.getAge());
		employeeService.saveEmployee(employeeDto);
	}

	@DeleteMapping("/{employeeId}")
	public void deleteEmployee(@PathVariable long employeeId) {
		employeeService.deleteEmployee(employeeId);
	}

	@GetMapping("/byNames")
	public List<Employee> getEmployeesByNames(@RequestParam String firstName, @RequestParam String lastName) {
		return employeeService.getEmployeesByNames(firstName, lastName);
	}

	@GetMapping("/byFirstName")
	public List<Employee> getEmployeesByNames(@RequestParam String firstName) {
		return employeeService.getEmployeesFisrtName(firstName, firstName);
	}

	@GetMapping("/byFirstNameContains")
	public List<Employee> getEmployeesByNamesContains(@RequestParam String firstName) {
		return employeeService.getEmployeesByNamesContains(firstName);
	}

	@GetMapping("/byFirstNameAsc")
	public List<Employee> getEmployeesByNamesAsc() {
		return employeeService.getEmployeesByFirstNameSort();
	}

	@GetMapping("/byFirstNameDesc")
	public List<Employee> getEmployeesByNamesDesc() {
		return employeeService.getEmployeesByFirstNameDesc();
	}

	@GetMapping("/byFirstNameContainsByOrder")
	public List<Employee> getFirstNameContainsByOrder(@RequestParam String firstName) {
		return employeeService.getFirstNameContainsByOrder(firstName);
	}

	@GetMapping("/pagination")
	public List<Employee> getEmployeesForPagination(@RequestParam int pageNumber, @RequestParam int pageSize) {
		return employeeService.getEmployeesForPagination(pageNumber, pageSize);
	}

	@GetMapping("/paginationFirstNameSort")
	public List<Employee> getEmployeesForPaginationFirstNameSort(@RequestParam int pageNumber,
			@RequestParam int pageSize) {
		return employeeService.getEmployeesForPaginationFirstNameSort(pageNumber, pageSize);
	}

	@GetMapping("/paginationFirstNameLastNameSort")
	public List<Employee> getEmployeesForPaginationFirstNameLastNameSort(@RequestParam int pageNumber,
			@RequestParam int pageSize) {
		return employeeService.getEmployeesForPaginationFirstNameLastNameSort(pageNumber, pageSize);
	}

	@GetMapping("/byNamesByHql")
	public List<Employee> getEmployeesByNamesByHql(@RequestParam String firstName, @RequestParam String lastName) {
		return employeeService.getEmployeesByNamesByHql(firstName, lastName);
	}

	@GetMapping("/byNamesByNativeQuery")
	public List<Employee> getEmployeesByNamesByNativeQuery(@RequestParam String firstName,
			@RequestParam String lastName) {
		return employeeService.getEmployeesByNamesByNativeQuery(firstName, lastName);
	}

	@GetMapping("/age/between/{startAge}/{endAge}")
	public List<Employee> getEmployeesByAgeBetween(@PathVariable int startAge, @PathVariable int endAge) {
		return employeeService.getEmployeesByAgeBetween(startAge, endAge);
	}
}
