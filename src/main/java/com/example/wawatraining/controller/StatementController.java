package com.example.wawatraining.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawatraining.dto.TransactionsResponseDto;
import com.example.wawatraining.repository.TransactionRepository;
import com.example.wawatraining.service.TransactionService;

@RestController
@RequestMapping("/statements")
public class StatementController {

	@Autowired
	TransactionService transactionService;

	@Autowired
	TransactionRepository transactionRepository;

	private static Logger log = LoggerFactory.getLogger(StatementController.class);

	@PostMapping("/{month}/{year}")
	public TransactionsResponseDto statementProcess(@PathVariable String month, @PathVariable String year, long accountNumber)
			throws ParseException {

		TransactionsResponseDto transactionsResponseDto = transactionService.getAllByDates(month, year, accountNumber);
		log.info("featching from databaetrnsactionList " + transactionsResponseDto);
		transactionsResponseDto.setStatusCode(200);
		transactionsResponseDto.setStatusMessage("Success");
		return transactionsResponseDto;
	}

}
