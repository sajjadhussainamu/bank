package com.example.wawatraining.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainingClientResponseDto {

	private Long id;
	private String name;
	private String status;

}
