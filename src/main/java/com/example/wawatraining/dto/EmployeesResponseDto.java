package com.example.wawatraining.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeesResponseDto {

	private int statusCode;
	private String statusMessage;

	List<EmployeeResponseDto> employeeResponseDto = new ArrayList<>();

}
