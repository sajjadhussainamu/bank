package com.example.wawatraining.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionsResponseDto {

	private int statusCode;
	private String statusMessage;

	TransactionResponseDto transactionResponseDto = new TransactionResponseDto();

	List<TransactionResponseDto> transactionResponseDtos = new ArrayList<>();

}
