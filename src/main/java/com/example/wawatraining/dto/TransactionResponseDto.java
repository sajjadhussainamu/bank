package com.example.wawatraining.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionResponseDto {

	private long id;
	private long fromAccount;
	private long toAccount;
	private float amount;

}
