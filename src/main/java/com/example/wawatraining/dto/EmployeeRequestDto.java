package com.example.wawatraining.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeRequestDto {

	@NotNull
	private String firstName;

	@NotEmpty
	@NotBlank
	private String lastName;

	@NotNull
	@NotEmpty
	private int age;

}
