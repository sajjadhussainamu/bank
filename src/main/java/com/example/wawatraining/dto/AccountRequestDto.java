package com.example.wawatraining.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountRequestDto {

	private long accountNumber;

}
