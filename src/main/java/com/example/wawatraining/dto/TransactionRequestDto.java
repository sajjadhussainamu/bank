package com.example.wawatraining.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionRequestDto {

	private long fromAccount;

	private long toAccount;

}
