package com.example.wawatraining.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountResponsetDto {

	private long accountNumber;

	private float openingBalance;

	private int status;

}
