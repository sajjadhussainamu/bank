package com.example.wawatraining.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class CustomerResponseDto {

	private long id;

	private String firstName;

	private String lastName;

	private String city;

	private long phoneNumber;

	private String username;

	private List<AccountResponsetDto> accountDetails = new ArrayList<>();

	
}
