package com.example.wawatraining.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerRequestDto {
	private String firstName;

	private String lastName;

	private String city;

	private long phoneNumber;

	private String username;

	private String password;

}
