package com.example.wawatraining.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmployeeTrainingClientResponseDto {

	private Long employeeId;
	List<TrainingClientResponseDto> trainingResponseDto = new ArrayList<>();

}
