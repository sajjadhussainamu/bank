package com.example.wawatraining.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeResponseDto {

	private Long id;

	private String firstName;

	private String lastName;
	
	EmployeeTrainingClientResponseDto employeeTrainingClientResponseDto;


}
