package com.example.wawatraining.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.wawatraining.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	List<Transaction> findByFromAccountAndCreateDateBetween(long accountNumber, LocalDateTime sDate,
			LocalDateTime eDate);

	List<Transaction> findByToAccountAndCreateDateBetween(long accountNumber, LocalDateTime sDate, LocalDateTime eDate);

}
