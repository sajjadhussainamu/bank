package com.example.wawatraining.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.wawatraining.entity.Customer;

@Repository
public interface RegistraionRepository extends JpaRepository<Customer, Integer> {

	Optional<Customer> findByUsername(String username);

	List<Customer> findByUsernameOrPhoneNumber(String username, long phoneNumber);

	Optional<Customer> findByPhoneNumber(long phoneNumber);

}
