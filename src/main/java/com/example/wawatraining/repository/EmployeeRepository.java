package com.example.wawatraining.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.wawatraining.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	List<Employee> findByFirstNameAndLastName(String firstName, String lastName);

	List<Employee> findByFirstName(String firstName);

	List<Employee> findByFirstNameContains(String firstName);

	List<Employee> findByFirstNameContainsOrderByFirstNameAsc(String firstName);

	@Query("from Employee e where e.firstName= :firstName and lastName=:lastName")
	List<Employee> getEmployeesByNamesByHql(@Param("firstName") String firstName, @Param("lastName") String lastName);

	@Query(value = "select * from employee e where e.fisrt_name= :firstName and last_name=:lastName", nativeQuery = true)
	List<Employee> getEmployeesByNamesByNativeQuery(@Param("firstName") String firstName,
			@Param("lastName") String lastName);

	List<Employee> findByAgeBetween(int startAge, int endAge);

}
