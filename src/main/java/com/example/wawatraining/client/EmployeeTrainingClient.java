package com.example.wawatraining.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.wawatraining.dto.EmployeeTrainingClientResponseDto;

//@FeignClient(value = "employee-training-service", url = "http://localhost:8080/trainings")
@FeignClient(name = "http://TRAINING-SERVICE")
public interface EmployeeTrainingClient {

	@GetMapping("/trainings")
	public EmployeeTrainingClientResponseDto getTrainingClientByEmployeeId(@RequestParam("employeeId") Long employeeId);

}
