package com.example.wawatraining.util;

import java.time.LocalDateTime;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.wawatraining.entity.Account;
import com.example.wawatraining.entity.Customer;
import com.example.wawatraining.entity.Transaction;

public class Utility {

	private static Logger log = LoggerFactory.getLogger(Utility.class);

	public static Transaction setTransaction(Account updateFromAccount, Account updateToAccount, float amount) {

		Transaction tran = new Transaction();

		tran.setAmount(amount);
		tran.setFromAccount(updateFromAccount.getAccountNumber());
		tran.setToAccount(updateToAccount.getAccountNumber());
		log.info("" + tran);
		return tran;
	}

	public static Account setAccount(Customer customer) {
		Random random = new Random();
		StringBuffer str = new StringBuffer();
		for (int i = 0; i < 16; i++) {
			str.append(random.nextInt(10));
		}
		long accountNumber = Long.parseLong(new String(str));

		Account account = new Account();
		account.setAccountNumber(accountNumber);
		account.setCustomer(customer);
		account.setOpeningBalance(10000);
		account.setStatus(1);
		return account;
	}

	public static LocalDateTime setFromDate(String month, String year) {
		int monthNumber;
		try {
			monthNumber = Integer.parseInt(month);
		} catch (NumberFormatException nfe) {
			monthNumber = Utility.setMonthInt(month);
		}
		LocalDateTime fromDate = LocalDateTime.of(Integer.parseInt(year), monthNumber, 01, 00, 00, 00);
		return fromDate;

	}

	public static LocalDateTime setToDate(String month, String year) {
		LocalDateTime toDate;
		int monthNumber;
		try {
			monthNumber = Integer.parseInt(month);
		} catch (NumberFormatException nfe) {
			monthNumber = Utility.setMonthInt(month);
		}
		if (monthNumber != 2) {
			toDate = LocalDateTime.of(Integer.parseInt(year), monthNumber, 31, 23, 59, 59);
		} else {
			toDate = LocalDateTime.of(Integer.parseInt(year), monthNumber, 29, 23, 59, 59);
		}
		return toDate;

	}

	private static int setMonthInt(String month) {
		int monthNumber = 0;

		switch (month.toUpperCase()) {
		case "JANUARY":
			monthNumber = 1;
			break;
		case "FEBRUAY":
			monthNumber = 2;
			break;
		case "MARCH":
			monthNumber = 3;
			break;
		case "APRIL":
			monthNumber = 4;
			break;
		case "MAY":
			monthNumber = 5;
			break;
		case "JUNE":
			monthNumber = 6;
			break;
		case "JULY":
			monthNumber = 7;
			break;
		case "AUGUST":
			monthNumber = 8;
			break;
		case "SEPTEMBER":
			monthNumber = 9;
			break;
		case "OCTOBER":
			monthNumber = 10;
			break;
		case "NOVEMBER":
			monthNumber = 11;
			break;
		case "DECEMBER":
			monthNumber = 12;
			break;
		default:
			break;
		}
		return monthNumber;
	}

}
