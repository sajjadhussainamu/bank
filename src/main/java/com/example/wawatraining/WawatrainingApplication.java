package com.example.wawatraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
public class WawatrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(WawatrainingApplication.class, args);
	}

}
