package com.example.wawatraining.service;

import java.util.List;

import com.example.wawatraining.dto.EmployeeRequestDto;
import com.example.wawatraining.dto.EmployeeResponseDto;
import com.example.wawatraining.dto.EmployeesResponseDto;
import com.example.wawatraining.entity.Employee;

public interface EmployeeService {

	EmployeeResponseDto getEmployeeById(long employeeId);

	EmployeesResponseDto getEmployees();

	void saveEmployee(EmployeeRequestDto employeeDto);

	void deleteEmployee(long employeeId);

	List<Employee> getEmployeesByNames(String firstName, String lastName);

	List<Employee> getEmployeesFisrtName(String firstName, String firstName2);

	List<Employee> getEmployeesByNamesContains(String firstName);

	List<Employee> getEmployeesByFirstNameSort();

	List<Employee> getEmployeesByFirstNameDesc();

	List<Employee> getFirstNameContainsByOrder(String firstName);

	List<Employee> getEmployeesForPagination(int pageNumber, int pageSize);

	List<Employee> getEmployeesForPaginationFirstNameSort(int pageNumber, int pageSize);

	List<Employee> getEmployeesForPaginationFirstNameLastNameSort(int pageNumber, int pageSize);

	List<Employee> getEmployeesByNamesByHql(String firstName, String lastName);

	List<Employee> getEmployeesByNamesByNativeQuery(String firstName, String lastName);

	List<Employee> getEmployeesByAgeBetween(int startAge, int endAge);

}
