package com.example.wawatraining.service;

import com.example.wawatraining.dto.TransactionsResponseDto;
import com.example.wawatraining.entity.Account;

public interface TransactionService {

	TransactionsResponseDto transactionsProcess(Account account, Account account2, float amount);

	TransactionsResponseDto getAllByDates(String month, String year, long accountNumber);

}
