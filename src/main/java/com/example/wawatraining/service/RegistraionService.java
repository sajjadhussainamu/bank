package com.example.wawatraining.service;

import java.util.Optional;

import com.example.wawatraining.dto.CustomerRequestDto;
import com.example.wawatraining.dto.CustomerResponseDto;
import com.example.wawatraining.entity.Customer;

public interface RegistraionService {

	CustomerResponseDto createCustomer(CustomerRequestDto customerRequestDto);

	CustomerResponseDto getCustomerByUsername(String customerRequestDto);

	CustomerResponseDto getCustomerByUsernameORByPhoneNumber(CustomerResponseDto customerResponseDto);

	Optional<Customer> getCustomerByPhoneNumber(long phone);

}
