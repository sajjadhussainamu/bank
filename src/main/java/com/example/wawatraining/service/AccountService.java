package com.example.wawatraining.service;

import java.util.Optional;

import com.example.wawatraining.entity.Account;
import com.example.wawatraining.entity.Customer;

public interface AccountService {

	void saveAccount(Account account);

	Optional<Account> getAccountByAccountNumber(long formAccount);

	Account updateAccount(Account account, Account account2, float amount);

	Account depositeAmount(Account account, float amount);

	Account withdrawalAmount(Account account, float amount);

	String createAccount(Customer customer);

}
