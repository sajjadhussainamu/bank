package com.example.wawatraining.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.wawatraining.dto.AccountResponsetDto;
import com.example.wawatraining.dto.CustomerRequestDto;
import com.example.wawatraining.dto.CustomerResponseDto;
import com.example.wawatraining.entity.Account;
import com.example.wawatraining.entity.Customer;
import com.example.wawatraining.repository.RegistraionRepository;
import com.example.wawatraining.service.AccountService;
import com.example.wawatraining.service.RegistraionService;
import com.example.wawatraining.util.Utility;

@Service
@Transactional
public class RegistraionServiceImpl implements RegistraionService {

	@Autowired
	RegistraionRepository registraionRepository;

	@Autowired
	AccountService accountService;

	@Override
	public CustomerResponseDto createCustomer(CustomerRequestDto customerRequestDto) {
		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRequestDto, customer);

		Account account = Utility.setAccount(customer);
		customer.setStatus(1);

		registraionRepository.save(customer);
		accountService.saveAccount(account);

		CustomerResponseDto customerResponseDto = new CustomerResponseDto();
		BeanUtils.copyProperties(customer, customerResponseDto);

		AccountResponsetDto accountResponsetDto = new AccountResponsetDto();
		BeanUtils.copyProperties(account, accountResponsetDto);
		
		List<AccountResponsetDto> accountDetails = new ArrayList<>();
		accountDetails.add(accountResponsetDto);
		customerResponseDto.setAccountDetails(accountDetails);

		return customerResponseDto;
	}

	@Override
	public CustomerResponseDto getCustomerByUsername(String username) {
		Optional<Customer> customer = registraionRepository.findByUsername(username);

		CustomerResponseDto customerResponseDto = new CustomerResponseDto();
		if (!customer.isPresent())
			return null;
		BeanUtils.copyProperties(customer, customerResponseDto);
		return customerResponseDto;
	}

	@Override
	public CustomerResponseDto getCustomerByUsernameORByPhoneNumber(CustomerResponseDto customer) {
		 registraionRepository.findByUsernameOrPhoneNumber(customer.getUsername(), customer.getPhoneNumber());
		 return null;
	
	}

	@Override
	public Optional<Customer> getCustomerByPhoneNumber(long phoneNumber) {
		return registraionRepository.findByPhoneNumber(phoneNumber);

	}

}
