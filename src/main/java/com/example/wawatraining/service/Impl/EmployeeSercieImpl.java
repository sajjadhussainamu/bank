package com.example.wawatraining.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.example.wawatraining.client.EmployeeTrainingClient;
import com.example.wawatraining.dto.EmployeeRequestDto;
import com.example.wawatraining.dto.EmployeeResponseDto;
import com.example.wawatraining.dto.EmployeeTrainingClientResponseDto;
import com.example.wawatraining.dto.EmployeesResponseDto;
import com.example.wawatraining.entity.Employee;
import com.example.wawatraining.repository.EmployeeRepository;
import com.example.wawatraining.service.EmployeeService;

@Service
public class EmployeeSercieImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	EmployeeTrainingClient employeeTrainingClient;

	@Override
	public EmployeeResponseDto getEmployeeById(long employeeId) {
		Employee employee = employeeRepository.findById(employeeId).get();

		EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
		BeanUtils.copyProperties(employee, employeeResponseDto);

		EmployeeTrainingClientResponseDto employeeTrainingClientResponseDto = employeeTrainingClient
				.getTrainingClientByEmployeeId(employeeId);

		employeeResponseDto.setEmployeeTrainingClientResponseDto(employeeTrainingClientResponseDto);

		return employeeResponseDto;
	}

	@Override
	public EmployeesResponseDto getEmployees() {
		// public List<EmployeeResponseDto> getEmployees() {
		List<EmployeeResponseDto> employeeResponseDtos = new ArrayList<>();

		List<Employee> employees = employeeRepository.findAll();
		for (Employee employee : employees) {
			EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
			BeanUtils.copyProperties(employee, employeeResponseDto);
			employeeResponseDtos.add(employeeResponseDto);
		}
		EmployeesResponseDto employeesResponseDto = new EmployeesResponseDto();
		employeesResponseDto.setEmployeeResponseDto(employeeResponseDtos);
		// return employeeResponseDtos;
		return employeesResponseDto;
	}

	@Override
	public void saveEmployee(EmployeeRequestDto employeeDto) {
		Employee employee = new Employee();
		BeanUtils.copyProperties(employeeDto, employee);
		employeeRepository.save(employee);
	}

	@Override
	public void deleteEmployee(long employeeId) {
		employeeRepository.deleteById(employeeId);

	}

	@Override
	public List<Employee> getEmployeesByNames(String firstName, String lastName) {
		return employeeRepository.findByFirstNameAndLastName(firstName, lastName);
	}

	@Override
	public List<Employee> getEmployeesFisrtName(String firstName, String firstName2) {
		return employeeRepository.findByFirstName(firstName);
	}

	@Override
	public List<Employee> getEmployeesByNamesContains(String firstName) {
		return employeeRepository.findByFirstNameContains(firstName);
	}

	@Override
	public List<Employee> getEmployeesByFirstNameSort() {
		return employeeRepository.findAll(Sort.by(Direction.ASC, "firstName"));
	}

	@Override
	public List<Employee> getEmployeesByFirstNameDesc() {
		return employeeRepository.findAll(Sort.by(Direction.DESC, "firstName"));
	}

	@Override
	public List<Employee> getFirstNameContainsByOrder(String firstName) {
		return employeeRepository.findByFirstNameContainsOrderByFirstNameAsc(firstName);
	}

	@Override
	public List<Employee> getEmployeesForPagination(int pageNumber, int pageSize) {

		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
		return employeeRepository.findAll(pageRequest).getContent();
	}

	@Override
	public List<Employee> getEmployeesForPaginationFirstNameSort(int pageNumber, int pageSize) {

		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by(Direction.DESC, "firstName"));
		return employeeRepository.findAll(pageRequest).getContent();
	}

	@Override
	public List<Employee> getEmployeesForPaginationFirstNameLastNameSort(int pageNumber, int pageSize) {

		Sort.by(Direction.ASC, "firstName");
		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by(Direction.ASC, "lastName"));
		return employeeRepository.findAll(pageRequest).getContent();
	}

	@Override
	public List<Employee> getEmployeesByNamesByHql(String firstName, String lastName) {
		return employeeRepository.getEmployeesByNamesByHql(firstName, lastName);
	}

	@Override
	public List<Employee> getEmployeesByNamesByNativeQuery(String firstName, String lastName) {
		return employeeRepository.getEmployeesByNamesByNativeQuery(firstName, lastName);
	}

	@Override
	public List<Employee> getEmployeesByAgeBetween(int startAge, int endAge) {
		return employeeRepository.findByAgeBetween(startAge, endAge);
	}

}
