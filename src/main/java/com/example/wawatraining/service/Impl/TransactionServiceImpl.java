package com.example.wawatraining.service.Impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.wawatraining.dto.TransactionResponseDto;
import com.example.wawatraining.dto.TransactionsResponseDto;
import com.example.wawatraining.entity.Account;
import com.example.wawatraining.entity.Transaction;
import com.example.wawatraining.repository.TransactionRepository;
import com.example.wawatraining.service.AccountService;
import com.example.wawatraining.service.TransactionService;
import com.example.wawatraining.util.Utility;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	AccountService accountService;
	private static Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

	@Override
	public TransactionsResponseDto transactionsProcess(Account updateFromAccount, Account updateToAccount,
			float amount) {

		accountService.updateAccount(updateFromAccount, updateToAccount, amount);
		Transaction transDetails = Utility.setTransaction(updateFromAccount, updateToAccount, amount);
		transactionRepository.save(transDetails);

		TransactionResponseDto transactionResponseDto = new TransactionResponseDto();
		BeanUtils.copyProperties(transDetails, transactionResponseDto);

		TransactionsResponseDto transactionResponseEntityDto = new TransactionsResponseDto();
		transactionResponseEntityDto.setTransactionResponseDto(transactionResponseDto);
		return transactionResponseEntityDto;
	}

	@Override
	public TransactionsResponseDto getAllByDates(String month, String year, long accountNumber) {
		LocalDateTime fromDate = Utility.setFromDate(month, year);
		LocalDateTime toDate = Utility.setToDate(month, year);

		log.info("datetime1 " + toDate);
		List<Transaction> fromTransaction = transactionRepository.findByFromAccountAndCreateDateBetween(accountNumber,
				fromDate, toDate);
		List<Transaction> ToTransaction = transactionRepository.findByToAccountAndCreateDateBetween(accountNumber,
				fromDate, toDate);
		List<Transaction> transactions = Stream.concat(fromTransaction.stream(), ToTransaction.stream())
				.collect(Collectors.toList());

		List<TransactionResponseDto> transactionResponseDtos = new ArrayList<>();
		for (Transaction transaction : transactions) {
			TransactionResponseDto transactionResponseDto = new TransactionResponseDto();
			BeanUtils.copyProperties(transaction, transactionResponseDto);
			transactionResponseDtos.add(transactionResponseDto);
		}
		TransactionsResponseDto transactionsResponseDto = new TransactionsResponseDto();
		transactionsResponseDto.setTransactionResponseDtos(transactionResponseDtos);
		return transactionsResponseDto;

	}

}
