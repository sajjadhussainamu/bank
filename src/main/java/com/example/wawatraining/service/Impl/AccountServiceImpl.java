package com.example.wawatraining.service.Impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.wawatraining.entity.Account;
import com.example.wawatraining.entity.Customer;
import com.example.wawatraining.repository.AccountRepository;
import com.example.wawatraining.service.AccountService;
import com.example.wawatraining.service.RegistraionService;
import com.example.wawatraining.util.Utility;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	RegistraionService registraionService;

	@Override
	public void saveAccount(Account account) {
		accountRepository.save(account);
	}

	@Override
	public Optional<Account> getAccountByAccountNumber(long formAccount) {
		return accountRepository.findByAccountNumber(formAccount);

	}

	@Override
	public Account updateAccount(Account updateFromAccount, Account updateToAccount, float amount) {

		Account account = this.withdrawalAmount(updateFromAccount, amount);
		this.depositeAmount(updateToAccount, amount);
		return account;

	}

	@Override
	public Account depositeAmount(Account account, float amount) {

		account.setOpeningBalance(account.getOpeningBalance() + amount);
		account.setCustomer(account.getCustomer());
		accountRepository.save(account);
		return account;
	}

	@Override
	public Account withdrawalAmount(Account account, float amount) {

		account.setOpeningBalance(account.getOpeningBalance() - amount);
		account.setCustomer(account.getCustomer());
		accountRepository.save(account);
		return account;
	}

	@Override
	public String createAccount(Customer customer) {
		Optional<Customer> customerCheck = registraionService.getCustomerByPhoneNumber(customer.getPhoneNumber());

		if (!customerCheck.isPresent())
			return "user is not available please register first";

		Account account = Utility.setAccount(customerCheck.get());
		accountRepository.save(account);
		return "success";

	}

}
